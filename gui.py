#!/usr/bin/env python3

import os
import wx

class FrameMain(wx.Frame):
    def __init__(self, app):
        wx.Frame.__init__(self, parent = None, title = "GiNkS's app", size = (720, 240),
            style = wx.DEFAULT_FRAME_STYLE & ~(wx.RESIZE_BORDER |  wx.MAXIMIZE_BOX))

        self.app = app

        # initialize background panel and its sizers
        hSzrBG = wx.BoxSizer(wx.HORIZONTAL)
        vSzrBG = wx.BoxSizer(wx.VERTICAL)
        pnlBG = wx.Panel(self)
        #pnlBG.SetBackgroundColour(wx.Colour(wx.WHITE))

        # initialize sizers
        vSzr = wx.BoxSizer(wx.VERTICAL)
        vSzrBtm = wx.BoxSizer(wx.VERTICAL)
        hSzrBtm = wx.BoxSizer(wx.HORIZONTAL)
        vSzrBtmR = wx.BoxSizer(wx.VERTICAL)
        hSzrTop = wx.FlexGridSizer(5)

        # initialize windows for top sizer
        txtDirInput = wx.StaticText(pnlBG, label = "Choose input directory:")
        self.fldDirInput = wx.TextCtrl(pnlBG, size = (460, -1), style = wx.TE_PROCESS_ENTER)
        self.btnDirInputDialog = wx.Button(pnlBG, label = "...", size = (40, -1))
        txtDirOutput = wx.StaticText(pnlBG, label = "Choose output directory:")
        self.fldDirOutput = wx.TextCtrl(pnlBG, size = (460, -1), style = wx.TE_PROCESS_ENTER)
        self.btnDirOutputDialog = wx.Button(pnlBG, label = "...", size = (40, -1))

        # fill top sizer
        hSzrTop.Add(txtDirInput, 0, wx.ALIGN_CENTER_VERTICAL | wx.BOTTOM, 5)
        hSzrTop.AddSpacer(8)
        hSzrTop.Add(self.fldDirInput, 0, wx.ALIGN_CENTER_VERTICAL | wx.BOTTOM, 5)
        hSzrTop.AddSpacer(8)
        hSzrTop.Add(self.btnDirInputDialog, 0, wx.ALIGN_CENTER_VERTICAL | wx.BOTTOM, 5)
        hSzrTop.Add(txtDirOutput, 0, wx.ALIGN_CENTER_VERTICAL)
        hSzrTop.AddSpacer(-1)
        hSzrTop.Add(self.fldDirOutput, 0, wx.ALIGN_CENTER_VERTICAL)
        hSzrTop.AddSpacer(-1)
        hSzrTop.Add(self.btnDirOutputDialog, 0, wx.ALIGN_CENTER_VERTICAL)

        # initialize windows for bottom sizer
        self.fldLog = wx.TextCtrl(pnlBG, value = "",
            style = wx.TE_READONLY | wx.TE_MULTILINE)

        # initialize windows for bottom right sizer
        self.btnRun = wx.Button(pnlBG, label = "Run treatment")
        self.btnStop = wx.Button(pnlBG, label = "Stop treatment")
        self.btnAbout = wx.Button(pnlBG, label = "About")

        # fill bottom right sizer
        vSzrBtmR.AddStretchSpacer(1)
        vSzrBtmR.Add(self.btnRun, 0, wx.ALIGN_CENTER_HORIZONTAL)
        vSzrBtmR.AddSpacer(5)
        vSzrBtmR.Add(self.btnStop, 0, wx.ALIGN_CENTER_HORIZONTAL)
        vSzrBtmR.AddSpacer(5)
        vSzrBtmR.Add(self.btnAbout, 0, wx.ALIGN_CENTER_HORIZONTAL)
        vSzrBtmR.AddStretchSpacer(1)

        # fill bottom horizontal sizer
        hSzrBtm.Add(self.fldLog, 4, wx.EXPAND)
        hSzrBtm.AddSpacer(10)
        hSzrBtm.Add(vSzrBtmR, 1, wx.EXPAND)

        # fill bottom vertical sizer
        vSzrBtm.Add(hSzrBtm, 1, wx.EXPAND)

        # fill main sizer
        vSzr.AddSpacer(10)
        vSzr.Add(hSzrTop, 0, wx.ALIGN_CENTER_HORIZONTAL)
        vSzr.AddSpacer(10)
        vSzr.Add(vSzrBtm, 1, wx.EXPAND | wx.RIGHT | wx.LEFT, 10)
        vSzr.AddSpacer(10)

        # activate main sizer on background panel
        pnlBG.SetSizer(vSzr)

        # fill background sizer
        vSzrBG.Add(pnlBG, 1, wx.EXPAND)
        hSzrBG.Add(vSzrBG, 1, wx.EXPAND)

        # activate background sizer on this frame
        self.SetSizer(hSzrBG)

        # bind actions and controls
        self.btnDirInputDialog.Bind(wx.EVT_BUTTON, self.OnDirInputDialog)
        self.fldDirInput.Bind(wx.EVT_CHAR, self.OnFldDirInputEnter)
        self.btnDirOutputDialog.Bind(wx.EVT_BUTTON, self.OnDirOutputDialog)
        self.fldDirOutput.Bind(wx.EVT_CHAR, self.OnFldDirOutputEnter)
        self.btnRun.Bind(wx.EVT_BUTTON, self.OnRun)
        self.btnStop.Bind(wx.EVT_BUTTON, self.OnStop)

        # declare and initialize some triggers, spruce up some windows
        self.trDirInput = False
        self.trDirOutput = False

        # self.fldLog.SetCanFocus(False)
        # self.fldLog.DisableFocusFromKeyboard()

        self.btnStop.Enable(False)
        self.btnAbout.Enable(False)

        # show this frame
        self.Show(True)

        # some log thing
        self.fldLog.AppendText("Welcome to GiNkS's app!\n")

    def OnDirInputDialog(self, e):
        dlg = wx.DirDialog(self, "Choose input directory", "", wx.DD_DIR_MUST_EXIST)
        if dlg.ShowModal() == wx.ID_OK:
            self.fldDirInput.ChangeValue(dlg.GetPath())
            if self.fldDirOutput.IsModified() == False:
                self.fldDirOutput.ChangeValue(dlg.GetPath())
                self.fldDirOutput.MarkDirty()
        dlg.Destroy()
        self.fldDirOutput.SetFocus();

    def OnDirOutputDialog(self, e):
        dlg = wx.DirDialog(self, "Choose output directory", "", wx.DD_DIR_MUST_EXIST)
        if dlg.ShowModal() == wx.ID_OK:
            self.fldDirOutput.ChangeValue(dlg.GetPath())
            self.fldDirOutput.MarkDirty()
        dlg.Destroy()
        self.btnRun.SetFocus();

    def OnFldDirInputEnter(self, e):
        if e.KeyCode == wx.WXK_RETURN:
            self.fldDirOutput.SetFocus()
            return
        e.Skip()

    def OnFldDirOutputEnter(self, e):
        if e.KeyCode == wx.WXK_RETURN:
            self.OnRun(e)
            return
        e.Skip()

    def AssumeRun(self):
        self.fldDirInput.Enable(False)
        self.btnDirInputDialog.Enable(False)
        self.fldDirOutput.Enable(False)
        self.btnDirOutputDialog.Enable(False)
        self.btnRun.Enable(False)
        # self.btnStop.Enable(True)

        # self.btnStop.SetFocus()

    def AssumeStop(self):
        self.fldDirInput.Enable(True)
        self.btnDirInputDialog.Enable(True)
        self.fldDirOutput.Enable(True)
        self.btnDirOutputDialog.Enable(True)
        self.btnRun.Enable(True)
        # self.btnStop.Enable(False)

        self.btnRun.SetFocus()

    def OnRun(self, e):
        if not self.fldDirInput.GetValue():
            wx.MessageBox("Input dir is not specified.", "Error", wx.ICON_ERROR)
            self.fldDirInput.SetFocus()
            return
        if not os.path.isdir(self.fldDirInput.GetValue()):
            wx.MessageBox("Specified input directory does not exist.", "Error", wx.ICON_ERROR)
            self.fldDirInput.SetFocus()
            return

        if not self.fldDirOutput.GetValue():
            wx.MessageBox("Output dir is not specified.", "Error", wx.ICON_ERROR)
            self.fldDirOutput.SetFocus()
            return
        if not os.path.isdir(self.fldDirOutput.GetValue()):
            wx.MessageBox("Specified output directory does not exist.", "Error", wx.ICON_ERROR)
            self.fldDirOutput.SetFocus()
            return

        self.AssumeRun()
        self.fldLog.AppendText("run treatment\n")
        wx.MessageBox("Program will be closed. Wait until the treatment is done in background",
                      "Going to run", wx.ICON_INFORMATION)

        self.Show(False)
        self.app.ExitMainLoop()

    def OnStop(self, e):
        self.AssumeStop()
        self.fldLog.AppendText("treatment was interrupted by user\n")

app = wx.App(False)
frameMain = FrameMain(app)
app.MainLoop()

print("input directory path \t --\t " + frameMain.fldDirOutput.GetValue())
print("output directory path\t --\t " + frameMain.fldDirInput.GetValue())
